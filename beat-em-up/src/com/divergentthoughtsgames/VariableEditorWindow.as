package com.divergentthoughtsgames 
{
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import org.flixel.FlxG;
	
	import com.bit101.utils.MinimalConfigurator;
	
	import com.bit101.components.*;
	
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class VariableEditorWindow 
	{
		private var debugConfig:MinimalConfigurator;
		
		private var player:Player;
		
		/**
		 * Creates a window that can be used to edit in-game variables.
		 */
		public function create(player:Player) : MinimalConfigurator
		{
			Component.initStage(FlxG.stage);
			this.player = player;
             
            var xml:XML = <comps>
							<Window width="215" height="150" alpha="0.925" title="Editor" hasMinimizeButton="true" minimized="true">
								<Panel x="0" y="0" width="210" height="125">
									<HBox x="10" y="5">
										<VBox>
											<Label text="Player Max X Speed:" width="125" event="keyup:onKeyUp"/>
											<Label text="Player Max Y Speed:"/>
											<Label text="Player Deceleration X:"/>
											<Label text="Player Deceleration Y:"/>
											<PushButton label="Update" event="click:onClick"/>
										</VBox>
										<VBox spacing="7">
											<InputText id="playerMaxVelocityX" width="40" event="keyUp:onKeyUp"/>
											<InputText id="playerMaxVelocityY" width="40"/>
											<InputText id="playerDecelerationX" width="40"/>
											<InputText id="playerDecelerationY" width="40"/>
											<PushButton id="boundingBox" label="Bounding Box" width="65"/>
										</VBox>
									</HBox>
								</Panel>
							</Window>
						</comps>;
             
            debugConfig = new MinimalConfigurator(FlxG.stage, this);
            debugConfig.parseXML(xml);
			
			setInitialValues();
			
			debugConfig.getCompById("boundingBox").addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void {
				FlxG.visualDebug = !FlxG.visualDebug;
			});
			
			FlxG.mouse.show();
			
			return debugConfig;
		}
		
		private function setInitialValues() : void
		{
			(debugConfig.getCompById("playerMaxVelocityX") as InputText).text = player.maxVelocity.x.toString();
			(debugConfig.getCompById("playerMaxVelocityX") as InputText).restrict = "0123456789";
			(debugConfig.getCompById("playerMaxVelocityY") as InputText).text = player.maxVelocity.y.toString();
			(debugConfig.getCompById("playerMaxVelocityY") as InputText).restrict = "0123456789";
			(debugConfig.getCompById("playerDecelerationX") as InputText).text = player.drag.x.toString();
			(debugConfig.getCompById("playerDecelerationY") as InputText).text = player.drag.y.toString();
		}
		
		public function onKeyUp(event:KeyboardEvent):void
        {
			if (event.keyCode == 13)
			{
			}
        }
		
		public function onClick(event:MouseEvent):void
		{
			player.maxVelocity.x = Number((debugConfig.getCompById("playerMaxVelocityX") as InputText).text);
			player.maxVelocity.y = Number((debugConfig.getCompById("playerMaxVelocityY") as InputText).text);
			player.drag.x = Number((debugConfig.getCompById("playerDecelerationX") as InputText).text);
			player.drag.y = Number((debugConfig.getCompById("playerDecelerationY") as InputText).text);
		}
	}
}