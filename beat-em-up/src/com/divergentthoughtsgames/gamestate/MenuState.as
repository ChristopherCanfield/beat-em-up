package com.divergentthoughtsgames.gamestate {
	import org.flixel.FlxState;
	import org.flixel.FlxButton;
	import org.flixel.FlxG;
	
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class MenuState extends FlxState
	{
		private var startButton:FlxButton;
 
        public function MenuState()
        {
        }
 
        override public function create():void
        {
           // FlxG.mouse.show();
           // startButton = new FlxButton(900 / 2 - 40, 656 / 2, "Play!", startGame);
           // add(startButton);
		   
		   // Switch directly to the game; keep this class to make it simpler to add a menu.
		   startGame();
        }
 
        private function startGame():void
        {
            FlxG.mouse.hide();
            FlxG.switchState(new PlayState());
        }
	}

}