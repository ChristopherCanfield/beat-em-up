package com.divergentthoughtsgames.util 
{
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class MathUtils
	{
		public static function lerp(min: Number, max: Number, value: Number): Number
		{
			return (min + (max - min) * value);
		}
	}

}