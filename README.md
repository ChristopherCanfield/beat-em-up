# Beat-Em-Up Template #

An ActionScript 3 template for prototyping beat-em-up style games. Uses the Flixel game framework.

[Try it out.](http://christopherdcanfield.com/projects/ludumdare/30/templates/beat-em-up/)

![beat-em-up.png](https://bitbucket.org/repo/RAdMEL/images/4195384091-beat-em-up.png)

Christopher D. Canfield  
September 2014